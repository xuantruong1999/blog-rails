require 'test_helper'

class StaticsPageControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get statics_page_home_url
    assert_response :success
  end

end
