class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create

    @comment = Comment.new
    @comment.post_id = params[:comment][:post_id]
    @comment.user_id = current_user.id
    @comment.content = params[:comment][:content]

    if @comment.save
      flash[:success] = 'Comment successfully'
      respond_to do |format|
        format.js
      end

    else
      @post = @comment.post
      render 'posts/show'
    end
  end

  def destroy

    @comment = Comment.find(params[:id])
    @comment.delete
    redirect_to post_path(@comment.post)
  end

  private

  def comment_params
    params.require(:comment).permit :content, :post_id
  end
end
