class PostsController < ApplicationController

  def index
    @posts = Post.paginate(page: params[:page], per_page: 2)

  end

  def show
    @post = Post.find_by(params[:id])
    @comment = Comment.new
  end

  def new

    @post = Post.new

    # params[:tags] # 'Java PHP OOP'
    # tags = params[:tags].split(' ') # ['Java', 'PHP', 'OOP']
    #
    # tags.each do |tag_name|
    #   @post.tags.new name: tag_name
    # end
    #
    # if @post.save
    # else
    # end
  end

  def create
    @post = Post.new
    @post.user_id = current_user.id
    @post.title = params[:post][:title]
    @post.content = params[:post][:content]
    if @post.save
      flash[:success] = 'Create post successfully'
      redirect_to posts_path
    else
      flash[:danger] = 'Create post failly'
    end
  # 'Java PHP OOP'
    tags = params[:post][:tags].split(' ') # ['Java', 'PHP', 'OOP']
    tags.each do |tag_name|
    @post.tags.new name: tag_name
    end
    if @post.save
      flash[:success] = "add tag name successfully"
  
    else
      flash[:alert] = "add tag name failly"
    end
  end
end
